
package controladores;

import entidades.Persona;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import modelos.PersonaModel;


@Named(value = "BPersona")
@SessionScoped
public class PersonaManagedBean implements Serializable {
    
    private PersonaModel personaModel  = new PersonaModel();
    private Persona persona = new Persona();
    
    
    
    
    private List<Persona> listado = new ArrayList<>();

    public List<Persona> getListado() {
        return listado;
    }
    
    public String agregar(){
        listado.add(this.persona);
        this.persona= new Persona();
        
        return "index?faces-redirect=true";
    }
    
      public String getActualizar() {
        personaModel.editar(this.persona);
        this.persona = new Persona();
        
        return "index?faces-redirect=true";
    }
    
    
    
    
    
    public PersonaManagedBean() {
        //Inicializar objetos
    }
    
    
    public Persona getPersonaModel() {
        
        return  personaModel.consultar();
    }


    /**
     * @return the persona
     */
    public Persona getPersona() {
        return persona;
    }

    /**
     * @param persona the persona to set
     */
    public void setPersona(Persona persona) {
        this.persona = persona;
    }
       
       
    
    
    
    
    
    
}
